drop table if exists uls.GIS_TLO CASCADE;

create table uls.GIS_TLO AS
        SELECT *,
	lat_degrees + lat_minutes/60. + lat_seconds / 3600. as lat_total,
	-1 * (long_degrees + long_minutes/60. + long_seconds / 3600.0) as long_total,
	ST_MakePoint(-1 * (long_degrees + long_minutes/60. + long_seconds / 3600.0),
		     lat_degrees + lat_minutes/60. + lat_seconds / 3600. ) as geopoint
	from PUBACC_LO
	WHERE lat_direction = 'N' and long_direction='W'
   ;
create index gis_tlo_csindex on GIS_TLO(call_sign);

drop table if exists uls.GIS_XMIT_LINKS CASCADE;
CREATE TABLE GIS_XMIT_LINKS AS
       select
	lo1.call_sign,
	lo1.lat_total as xmit_lat_total,
	lo1.long_total as xmit_long_total,
	lo2.lat_total as recv_lat_total,
	lo2.long_total as recv_long_total,
	ST_Distance(lo1.geopoint, lo2.geopoint) as link_dist,
	ST_MakeLine(lo1.geopoint, lo2.geopoint) as geoline
	FROM GIS_TLO as lo1,
	     GIS_TLO as lo2
	WHERE lo1.call_sign = lo2.call_sign
	      AND lo1.location_class_code = 'T'
      	      AND lo2.location_class_code = 'R'
	LIMIT 1000;
;

drop table if exists uls.GIS_EM_REDUCED CASCADE;

create  table uls.GIS_EM_REDUCED AS
	SELECT
	    call_sign, frequency_assigned, location_number, count(*) as modulation_count
	FROM PUBACC_EM
	GROUP BY 
	    call_sign, frequency_assigned, location_number
;
create index gis_em_reduced_csindex on GIS_EM_REDUCED(call_sign);
create index gis_em_reduced_faindex on GIS_EM_REDUCED(frequency_assigned);


drop table if exists uls.GIS_EM_LO_XMIT CASCADE;

create table uls.GIS_EM_LO_XMIT AS
       SELECT
	lo1.unique_system_identifier as unique_system_identifier,
	lo1.call_sign as call_sign,
	lo1.location_number as location_number,
	frequency_assigned,
	location_class_code,
	lat_total, lat_direction,
	long_total, long_direction,
	geopoint,
	modulation_count
    from GIS_TLO as lo1,
    	 GIS_EM_REDUCED as em1
    WHERE lo1.call_sign = em1.call_sign AND lo1.location_number = em1.location_number
    	  and   location_class_code = 'T' 
    ORDER BY call_sign, location_number, frequency_assigned
;

create index gis_em_lo_lindex on GIS_EM_LO_XMIT(location_number);
create index gis_em_lo_idindex on GIS_EM_LO_XMIT(unique_system_identifier);
create index gis_em_lo_csindex on GIS_EM_LO_XMIT(call_sign);
create index gis_em_lo_fqindex on GIS_EM_LO_XMIT(frequency_assigned);
create index gis_em_lo_geoindex on GIS_EM_LO_XMIT USING GIST(geopoint);
vacuum analyze GIS_EM_LO_XMIT;


CREATE TABLE GIS_TOWER_CO AS
       SELECT
	registration_number, unique_system_identifier,
	latitude_degrees + latitude_minutes/60. + latitude_seconds / 3600. as latitude_total,
	-1 * (longitude_degrees + longitude_minutes/60. + longitude_seconds / 3600.0) as longitude_total,
	ST_MakePoint(-1 * (longitude_degrees + longitude_minutes/60. + longitude_seconds) / 3600.0,
		     latitude_degrees + latitude_minutes/60. + latitude_seconds / 3600. )
	as geopoint
	FROM TOWER_PUBACC_CO
	WHERE latitude_direction = 'N' and longitude_direction='W' ;
create index gis_tower_co_rnindex on GIS_TOWER_CO(registration_number);
create index gis_tower_co_usiindex on GIS_TOWER_CO(unique_system_identifier);
create index gis_tower_co_geoindex on GIS_TOWER_CO USING GIST(geopoint);


vacuum analyze GIS_TOWER_CO;
