#!/usr/bin/python

#
# parse a CSV file separate by vertical bars possibly overcoming
# split lines due to FCC dumping process.
#

import os,sys

lines = sys.stdin.read().splitlines()

def clean(x):
    x = x.replace('\r\n','')
    x = x.replace('\r', '')
    x = x.replace('"','')
    x = x.replace("'",'')
    return x

def isnumber(x):
    try:
        y = float(x)
        return True
    except:
        return False

def quotify(x):
    if (x.count('"') + x.count("'")) > 0:
        x = x.replace("'", "")
        x = x.replace('"', '')
    return x

numlines = len(lines)

#
# Determine the number of intended fields based on the first entry
#
line = clean(lines[0])
fields = line.split('|')
numfields = len(fields)

i = 0
while (i < numlines):
    line = lines[i]
    nf = len(line.split('|'))
    #
    # An entry may contain newlines and be split across
    # multiple input lines. Keep merging lines until
    # we have enough fields...
    #
    while nf < numfields:
        lines[i+0] = clean( lines[i+0] )
        lines[i+1] = clean( lines[i+1] )

        line = lines[i+0] + lines[i+1]
        i = i + 1
        lines[i] = line
        nf = len(line.split('|'))
        
    if nf == numfields:
        line = lines[i+0]
        ls1 = line.split('|')
        line = '|'.join([quotify(x) for x in ls1])
        print line
    else:
        print i, "line is ", lines[i+0], nf
        os.abort()
    i = i + 1

