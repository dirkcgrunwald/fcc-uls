#!/bin/sh -x

set -e

# Perform all actions as $POSTGRES_USER
export PGUSER="$POSTGRES_USER"


createdb --user $PGUSER $TIGER_DB
psql --user $PGUSER -d $TIGER_DB -c "CREATE EXTENSION postgis;"
psql --user $PGUSER -d $TIGER_DB -c "CREATE EXTENSION postgis_topology;"
psql --user $PGUSER -d $TIGER_DB -c "CREATE EXTENSION fuzzystrmatch;"
psql --user $PGUSER -d $TIGER_DB -c "CREATE EXTENSION postgis_tiger_geocoder;"

mkdir "/gisdata"
chown 0777 /gisdata

psql --user $PGUSER -d $TIGER_DB -o /gisdata/temp.sh -c "SELECT loader_generate_script(ARRAY['DC','CO','UT','NV','CA'], 'sh');"

sed 's/\s*+$//' /gisdata/temp.sh | tail -n +3 | head -n -2 > /gisdata/generate.sh
rm /gisdata/temp.sh
chmod +x /gisdata/generate.sh

sed -i -e 's|PGBIN=/usr/pgsql-9.0/bin|PGBIN=/usr/bin|' /gisdata/generate.sh
sed -i -e "s|PGUSER=postgres|PGUSER=$POSTGRES_USER|"  /gisdata/generate.sh
#
# Eliminate PGHOST so local connection is used..
#
sed -i -e "/PGHOST=localhost/d"  /gisdata/generate.sh
sed -i -e "s|PGPASSWORD=yourpasswordhere|PGPASSWORD=$POSTGRES_PASSWORD|"  /gisdata/generate.sh
sed -i -e "s|PGDATABASE=geocoder|PGDATABASE=$TIGER_DB|"  /gisdata/generate.sh
sed -i -e "s|ftp2.census.gov|$TIGER_CENSUSFTPMIRROR|"  /gisdata/generate.sh

ps augxww

cd /gisdata
echo "Modified generate.sh follows:"
cat ./generate.sh
./generate.sh

psql --user $PGUSER -d $TIGER_DB -c  "SELECT install_missing_indexes();"
psql --user $PGUSER -d $TIGER_DB -c  "vacuum analyze verbose tiger.addr;"
psql --user $PGUSER -d $TIGER_DB -c  "vacuum analyze verbose tiger.edges;"
psql --user $PGUSER -d $TIGER_DB -c  "vacuum analyze verbose tiger.faces;"
psql --user $PGUSER -d $TIGER_DB -c  "vacuum analyze verbose tiger.featnames;"
psql --user $PGUSER -d $TIGER_DB -c  "vacuum analyze verbose tiger.place;"
psql --user $PGUSER -d $TIGER_DB -c  "vacuum analyze verbose tiger.cousub;"
psql --user $PGUSER -d $TIGER_DB -c  "vacuum analyze verbose tiger.county;"
psql --user $PGUSER -d $TIGER_DB -c  "vacuum analyze verbose tiger.state;"
#
# test for success
#
psql --user $PGUSER -d $TIGER_DB -c "SELECT g.rating, ST_X(geomout) As lon, ST_Y(geomout) As lat, (addy).* FROM geocode('1731 New Hampshire Avenue Northwest, Washington, DC 20010', 1) As g;" 
