#!/bin/bash

PGDB=tiger
PGUSER=postgres
PGSCHMEA=uls
FCC=../FCC

PSQL="psql -h sdr.cs.colorado.edu -p 5432 --user $PGUSER -d $PGDB"

function XLMICRO()
{
    echo "Load file Xlmicrowave/$1.dat..." ;
    cat $FCC/Xlmicrowave/$1.dat \
	| iconv -f LATIN1 -t utf-8 \
	| python ParseByFirstLine.py \
	| $PSQL -c "copy uls.PUBACC_$1 FROM STDIN DELIMITER '|' CSV;" ;
    $PSQL -c "vacuum analyze uls.PUBACC_$1;"
}

function TOWER()
{
    echo "Load file Xtower/$1.dat..."
    cat $FCC/Xtower/$1.dat \
	| iconv -f LATIN1 -t utf-8 \
	| python ParseByFirstLine.py \
	| $PSQL -c "copy uls.TOWER_PUBACC_$1 FROM STDIN DELIMITER '|' CSV;"
    $PSQL -c "vacuum analyze uls.TOWER_PUBACC_$1;"
}

$PSQL -c "drop database if exists ULS;"
$PSQL -c "DROP SCHEMA uls CASCADE"
$PSQL -c "CREATE SCHEMA uls"

#
# Because the table definitions no longer contain a schema tag, we need
# to force use of a default schema. The only way I could do this with
# individual psql commands was to force the change for the user and then
# try to put it back.
#

old_path=`$PSQL -o /dev/stdout -c "show search_path" | tail -3 | head -1`
echo "Old search path was $old_path"
$PSQL -c "ALTER ROLE $PGUSER SET search_path TO $PGSCHMEA,public,tiger"

cat $FCC/Tower.sql \
    | sed -e 's/datetime/varchar/' \
    | sed -e 's/varchar(255)/varchar/' \
    | $PSQL

cat $FCC/ULS.sql \
    | sed -e 's/datetime/varchar/' \
    | sed -e 's/varchar(255)/varchar/' \
    | sed -e 's/tinyint/int/g' \
    | sed -e 's/ offset / offsetf2 /g' \
    | $PSQL

XLMICRO a3 a3
XLMICRO AN AN
XLMICRO BC BC
XLMICRO BF BF
XLMICRO CO CO
XLMICRO CP CP
XLMICRO EM EM
XLMICRO EN EN
XLMICRO F2 F2
XLMICRO f3 F3
XLMICRO f5 F5
XLMICRO f6 F6
XLMICRO FF FF
XLMICRO FR FR
XLMICRO HD HD
XLMICRO HS HS
XLMICRO ir ir
XLMICRO L2 L2
XLMICRO LA LA
XLMICRO lc lc
XLMICRO LF LF
XLMICRO ll ll
XLMICRO LO LO
XLMICRO LS LS
XLMICRO MF MF
XLMICRO MK MK
XLMICRO MW MW
XLMICRO OP OP
XLMICRO p2 p2
XLMICRO PA PA
XLMICRO RC RC
XLMICRO SC SC
XLMICRO SF SF
XLMICRO tp tp

TOWER AT
TOWER CO
TOWER EC
#
# The FCC data has an error at line 1250153
# There is an extra verticle bar. Should be 1018 Highland Colony Parkway Suite|||Ridgeland|MS|39157||
#
# This is not handled automatically yet
#
TOWER EN
TOWER HS
TOWER RA
TOWER RE
TOWER RS
TOWER SC

$PSQL -c  "create index pubacc_lo_uindex on PUBACC_LO(unique_system_identifier);"
$PSQL -c  "create index pubacc_lo_ccindex on PUBACC_LO(location_class_code);"
$PSQL -c  "create index pubacc_em_uindex on PUBACC_EM(unique_system_identifier);"

$PSQL -c  "create index tower_pubacc_at_uindex on TOWER_PUBACC_AT(unique_system_identifier);"
$PSQL -c  "create index tower_pubacc_co_uindex on TOWER_PUBACC_CO(unique_system_identifier);"
$PSQL -c  "create index tower_pubacc_en_uindex on TOWER_PUBACC_EN(unique_system_identifier);"

$PSQL -c  "create index pubacc_lo_lindex on PUBACC_LO(location_number);"
$PSQL -c  "create index pubacc_em_lindex on PUBACC_EM(location_number);"

$PSQL -c  "create index pubacc_lo_csindex on PUBACC_LO(call_sign);"
$PSQL -c  "create index pubacc_em_csindex on PUBACC_EM(call_sign);"

$PSQL -c "ALTER ROLE $PGUSER SET search_path TO $old_path,$PGSCHMEA"
new_path=`$PSQL -o /dev/stdout -c "show search_path" | tail -3 | head -1`
echo "New search path is $new_path"
