#!/bin/bash

dbfile=$1

echo "dbfile is $dbfile"
rm -f $dbfile

cat Tower.sql | sqlite3 $dbfile
cat ULS.sql | sqlite3 $dbfile
echo ".import Xtower/CO.dat TOWER_PUBACC_CO" | sqlite3 $dbfile
echo ".import Xlmicrowave/EM.dat PUBACC_EM" | sqlite3 $dbfile
echo ".import Xlmicrowave/FR.dat PUBACC_FR" | sqlite3 $dbfile
echo ".import Xlmicrowave/AN.dat PUBACC_AN" | sqlite3 $dbfile
echo ".import Xlmicrowave/LO.dat PUBACC_LO" | sqlite3 $dbfile

echo "create index pubacc_lo_uindex on PUBACC_LO(unique_system_identifier);" | sqlite3 $dbfile
echo "create index pubacc_lo_ccindex on PUBACC_LO(location_class_code);" | sqlite3 $dbfile
echo "create index pubacc_em_uindex on PUBACC_EM(unique_system_identifier);" | sqlite3 $dbfile
echo "create index tower_pubacc_co_uindex on TOWER_PUBACC_CO(unique_system_identifier);" | sqlite3 $dbfile

echo "create index pubacc_lo_lindex on PUBACC_LO(location_number);" | sqlite3 $dbfile
echo "create index pubacc_em_lindex on PUBACC_EM(location_number);" | sqlite3 $dbfile


echo "create index pubacc_lo_csindex on PUBACC_LO(call_sign);" | sqlite3 $dbfile
echo "create index pubacc_em_csindex on PUBACC_EM(call_sign);" | sqlite3 $dbfile


#
# Appears to not work?
#echo ".import Xlmicrowave/tp.dat PUBACC_TP" | sqlite3 $dbfile
