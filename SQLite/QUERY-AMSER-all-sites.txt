select
	frequency_assigned
	, unique_system_identifier
	, lat_total
	, lat_direction
	, long_total
	, long_direction
FROM AMSER_TABLE
      WHERE location_class_code = 'T'
      AND long_direction = "W" 
      AND lat_total != 0
      AND long_total != 0
ORDER BY frequency_assigned, lat_total, long_total;
