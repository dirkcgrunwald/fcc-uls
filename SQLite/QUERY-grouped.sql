select
	count(frequency_assigned),
	 lat_degrees + lat_minutes/60. + lat_seconds / 3600. as lat_total,
	 lat_direction,
	 long_degrees + long_minutes/60. + long_seconds / 3600.0 as long_total,
	  long_direction
   from PUBACC_EM JOIN PUBACC_LO 
where PUBACC_EM.unique_system_identifier == PUBACC_LO.unique_system_identifier
      AND frequency_assigned >= 6000 AND frequency_assigned <= 8000 AND long_direction = "W" 
GROUP BY lat_total, long_total;
