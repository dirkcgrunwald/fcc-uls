FCC ULS
========

This is a combination of python programs and shell scripts to download
data from the FCC ULS database, scripts to suck that data into an
either an SQLite3 or PostGIS database and some specific queries for
that data.

The project is divided into three parts, each held in a distinct
directory.

* The FCC directory has scripts to download and unpack the ULS schema,
  the Tower schema and download the licensed (as opposed to
  application) files for microwave services. It also downloads the
  tower tables. There is a makefile which will perform these steps.

* The SQLite directory uses the data from the FCC directory to
  populate an SQLite3 database.  The Makefile will automatically fetch
  the data, extract it into subdirectories, build the SQLite database
  (using import, so it's speedy) and then conduct a set of queries.

  The import is not completely clean. For example, when importing the
  licensed microwave AN table, I get messages such as

    + sqlite3 uls.sqlite
    Xlmicrowave/AN.dat:388223: unescaped " character
    Xlmicrowave/AN.dat:388303: unescaped " character

  The resulting uls.sqlite file is ~500MB. I found that downloading
  the data was the most time consuming step, although some complex
  queries can take a while. You may need to add additional index
  generation steps to MakeSQLite.sh to speed your queries (or put
  it all into a big boy database).

* The PostGIS directory uses the data from the FCC directory to
  populate a directory in a PostGIS (tested: postgres 9.4, postgis
  2.1) database. The data is cleaned in-situ (meaning that some of the
  files in the FCC will be modified). New tables are to cee

The bigger question (for me) is the relationship between the different
tables and the keys to use for joins. The FCC documents are a little
sparse on this.

