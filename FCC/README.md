The directory fetches and then contains the FCC ULS (universal license
system) database files needed by the SQLite3 and PostGIS components.

Information on the data schema is at http://wireless.fcc.gov/uls/index.htm?job=transaction&page=weekly

The SQL schema definitions are hand-edited versions of the schema on that page. A direct link to the FCC schema is at e.g. http://wireless.fcc.gov/uls/ebf/pa_ddef49.txt

Their schema was not directly usable by SQLite, ergo the
modifications. I have no idea if it can be directly used by PostGIS.
