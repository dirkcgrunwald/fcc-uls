import urllib
import os

ftploc = "ftp://wireless.fcc.gov/pub/uls/complete"

DIRS = {
    "tower" : [ "a_tower.zip" ]
    , "amicrowave" : [ "a_micro.zip" ]
    , "lmicrowave" : [ "l_micro.zip" ]
}

for dir in DIRS:
    print "dir is ", dir
    localdir = "X" + dir;
    os.system("rm -rf " + localdir)
    os.system("mkdir " + localdir)
    for zip in DIRS[dir]:
        local = localdir + "/" + zip
        urllib.urlretrieve(ftploc + "/" + zip, local)
        os.system("cd " + localdir + "; unzip " + zip)


